module.exports = {
  apps: [
    {
      name: 'nextstep',
      script: './index.js',
      env: {
        'NODE_ENV': 'production'
      }
    }
  ]
}
