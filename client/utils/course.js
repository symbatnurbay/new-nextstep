import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    addCourse: (data) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/course/add`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    editCourse: (data, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/course/edit/${id}`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getCourses: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/course/get/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getMyCourses: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/course/my`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    checkCourse: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/course/check/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    remove: (id) => {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/course/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    payment: (cryptogram, course, card) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/course/pay`, { cryptogram, course, card })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
