import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    addCategory: (data) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/category/add`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    editCategory: (data, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/category/edit/${id}`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getCategories: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/category/get`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    removeCategory: (id) => {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/category/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getCurrentCategory: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/category/one/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
