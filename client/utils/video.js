import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    getVideos: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/video/list/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getNextVideo: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/video/next/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getPrevVideo: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/video/prev/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addVideo: (data) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/video/add`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    editVideo: (data, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/video/edit/${id}`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    checkVideo: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/video/check/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteVideo: (video) => {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/video/${video}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
