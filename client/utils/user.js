import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    getCurrentUser: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/user/current`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    editProfile: (data) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/user/edit`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getUsers: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/user/all`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeAvatar: (id, file) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/user/avatar/${id}`, file)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteImage: (id) => {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/user/avatar/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
