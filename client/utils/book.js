import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    getBooks: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/book/list/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addBook: (data) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/book/add`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    editBook: (data, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/book/edit/${id}`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    checkBook: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/book/check/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getNextBook: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/book/next/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getPrevBook: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/book/prev/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getGoals: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/goals/get/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addGoal: (id, text) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/goals/add/${id}`, text)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    editGoal: (goal) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/goals/edit/${goal.book}`, goal)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    removeGoal: (id, goal) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/goals/remove/${id}`, { goal })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteBook: (book) => {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/book/${book}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getAllGoals: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/goals/all`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
