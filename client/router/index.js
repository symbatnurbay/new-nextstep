import VueRouter from 'vue-router'

import Registration from 'Pages/registration'
import Login from 'Pages/login'
import Landing from 'Pages/landing'
import Category from 'Pages/category'
import Courses from 'Pages/courses'
import ForgetPassword from 'Pages/forget'
import NewPassword from 'Pages/newpass'
import AddCourse from 'Pages/addCourse'
import AddVideo from 'Pages/addVideo'
import AddBook from 'Pages/addBook'
import OneVideo from 'Pages/oneVideo'
import OneBook from 'Pages/oneBook'
import OneCourse from 'Pages/oneCourse'
import Profile from 'Pages/profile'
import User from 'Pages/users'
import About from 'Pages/about'
import Auth from '../utils/auth'

const routes = [
  {
    path: '/',
    component: Landing,
    name: 'Landing'
  },
  {
    path: '/login',
    component: Login,
    name: 'Login'
  },
  {
    path: '/signup',
    component: Registration,
    name: 'Registration'
  },
  {
    path: '/category',
    component: Category,
    name: 'Category'
  },
  {
    path: '/category/:id',
    component: Courses,
    name: 'OneCategory'
  },
  {
    path: '/category/:id/:error',
    component: Courses,
    name: 'OneCategory'
  },
  {
    path: '/my-courses',
    component: Courses,
    name: 'MyCourses'
  },
  {
    path: '/add-course',
    component: AddCourse,
    name: 'AddCourse'
  },
  {
    path: '/edit-course/:id',
    component: AddCourse,
    name: 'EditCourse'
  },
  {
    path: '/one-course/:id',
    component: OneCourse,
    name: 'OneCourse'
  },
  {
    path: '/add-video',
    component: AddVideo,
    name: 'AddVideo'
  },
  {
    path: '/edit-video/:id',
    component: AddVideo,
    name: 'EditVideo'
  },
  {
    path: '/add-book',
    component: AddBook,
    name: 'AddBook'
  },
  {
    path: '/edit-book/:id',
    component: AddBook,
    name: 'EditBook'
  },
  {
    path: '/video/:id',
    component: OneVideo,
    name: 'OneVideo'
  },
  {
    path: '/book/:id',
    component: OneBook,
    name: 'OneBook'
  },
  {
    path: '/forget',
    component: ForgetPassword,
    name: 'ForgetPassword'
  },
  {
    path: '/newpass',
    component: NewPassword,
    name: 'NewPassword'
  },
  {
    path: '/profile',
    component: Profile,
    name: 'Profile'
  },
  {
    path: '/users',
    component: User,
    name: 'User'
  },
  {
    path: '/about',
    component: About,
    name: 'About'
  }
]

const router = new VueRouter({
  history: true,
  routes
})

router.beforeEach((to, from, next) => {
  if (Auth().isLoggedIn()) {
    if (to.path === '/login' || to.path === '/signup' || to.path === '/' || to.path === '/forget' || to.path === '/newpass') {
      return next('/category')
    }
    next()
  } else {
    if (to.path !== '/signup' && to.path !== '/login' && to.path !== '/forget' && to.path !== '/newpass' && to.path !== '/') {
      next('/')
    } else {
      next()
    }
  }
})

export default router
