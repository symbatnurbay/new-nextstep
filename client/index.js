import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App.vue'

import router from './router'

import Api from './utils/api'
import ApiUser from './utils/user'
import Auth from './utils/auth'
import ApiCourse from './utils/course'
import ApiBook from './utils/book'
import ApiVideo from './utils/video'
import store from './store'
import Notifications from 'vue-notification'
import VModal from 'vue-js-modal'
import VSelect from 'Components/select'
import VLoader from 'Components/loader'
const VueInputMask = require('vue-inputmask').default

Vue.use(Notifications)

Vue.use(VueRouter)
Vue.use(VModal)
Vue.use(VueInputMask)
Vue.component('v-select', VSelect)
Vue.component('v-loader', VLoader)

const apiInstance = new Api()
Vue.prototype.$api = apiInstance
Vue.prototype.$auth = new Auth()
Vue.prototype.$apiUser = new ApiUser()
Vue.prototype.$apiCourse = new ApiCourse()
Vue.prototype.$apiBook = new ApiBook()
Vue.prototype.$apiVideo = new ApiVideo()

const app = new Vue({
  router,
  store,
  ...App
})

app.$mount('#app')
