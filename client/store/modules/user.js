const state = {
  currentUser: {}
}

const getters = {
  getCurrentUser: state => state.currentUser
}

const mutations = {
  setCurrentUser (state, payload) {
    state.currentUser = payload
  }
}

const actions = {}

export default {
  state,
  getters,
  mutations,
  actions
}
