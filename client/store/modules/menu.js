const state = {
  currentNav: 'Category',
  currentPage: 'Все категории'
}

const getters = {
  getCurrentNav: state => state.currentNav,
  getCurrentPage: state => state.currentPage
}

const mutations = {
  changeCurrentNav (state, payload) {
    state.currentNav = payload
  },
  changeCurrentPage (state, payload) {
    state.currentPage = payload
  }
}

const actions = {}

export default {
  state,
  getters,
  mutations,
  actions
}
