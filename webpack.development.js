const webpack = require('webpack')
var path = require('path')

// var ExtractTextPlugin = require('extract-text-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    client: [
      './client/index.js',
      'webpack-hot-middleware/client?path=http://localhost:8000/__webpack_hmr'
    ]
  },
  resolve: {
    extensions: ['.css', '.vue', '.js'],
    mainFiles: ['index'],
    alias: {
      vue: 'vue/dist/vue.js',
      Components: path.resolve(__dirname, './client/components/'),
      Pages: path.resolve(__dirname, './client/pages/'),
      EventBus: path.resolve(__dirname, './client/eventBus.js')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          cssModules: {
            localIdentName: '[path][name]---[local]---[hash:base64:5]',
            camelCase: true
          }
        }
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              camelCase: true,
              localIdentName: '[local]--[hash:base64:5]',
              importLoaders: 1
            }
          },
          'postcss-loader'
        ]
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './client/template.html'
    })
  ],
  devtool: '#source-map'
}
