import nodemailer from 'nodemailer'
import Configs from '../config/config'

const sendMail = (to, subject = '', text = '', html = '', done) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: Configs.emailData.email,
      pass: Configs.emailData.password
    }
  })

  const mailOptions = {
    from: `NextStep <${Configs.emailData.email}>`,
    to,
    subject,
    text,
    html
  }

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) return done(err)
    console.log('Message sent:', info.messageId)
    done(null, 'Письмо отправлено')
  })
}

module.exports = {
  sendMail
}
