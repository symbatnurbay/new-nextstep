import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import User from '../models/user'

passport.use(new LocalStrategy({
  usernameField: 'email',
  passReqToCallback: true
}, (req, username, password, done) => {
  User.findOne({ $or: [
    { email: username },
    { username: username }
  ] }, (err, user) => {
    if (err) return done(err)
    if (!user) {
      return done(null, false, { email: 'Пользователь не найден' })
    }

    if (!user.validPassword(password)) {
      return done(null, false, { password: 'Неправильное имя пользователя или пароль' })
    }

    return done(null, user)
  })
}))
