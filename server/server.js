import express from 'express'
import passport from 'passport'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import Configs from './config/config'

import mongoose from 'mongoose'
// mongoose.connect(`mongodb://192.168.4.20:27017/${Configs.db.name}`)
mongoose.connect(`mongodb://localhost:27017/${Configs.db.name}`)
require('./utils/passport')
const app = express()

const server = app.listen(Configs.serverData.port, () => {
  server.keepAliveTimeout = 0
  console.log(`magic is happening on port ${Configs.serverData.port}`)
})

require('./controllers/authentication').createAdmin()

if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack')
  const webpackConfig = require('../webpack.development.js')
  const compiler = webpack(webpackConfig)

  app.use(require('webpack-dev-middleware')(compiler, {
    log: false,
    path: '../client/build/__webpack_hmr',
    heartbeat: 1000
  }))

  app.use(require('webpack-hot-middleware')(compiler))
}

app.use(bodyParser.urlencoded({ limit: '200mb', extended: true }))
app.use(bodyParser.json({ limit: '200mb', type: 'application/json' }))
app.use(morgan('dev'))
app.use(passport.initialize())

import index from './routes/index'
app.use('/api', index)

app.use(express.static('client/build'))
app.use('/images', express.static('static/images'))
app.use('/fonts', express.static('static/fonts'))
app.use('/files', express.static('static/files'))
app.use('/video', express.static('static/video'))

// Exporting server
export default { app }
