import passport from 'passport'
import User from '../models/user'
import mailsender from '../utils/mail'
import Configs from '../config/config'

const signup = (req, res) => {
  User.findOne({ $or: [{ email: req.body.email }, { username: req.body.username }] }, (err, u) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else if (u) {
      res.status(401).send({ username: 'Пользователь с таким email или логином существует', email: 'Пользователь с таким email или логином существует' })
    } else {
      const user = new User()
      user.username = req.body.username
      user.email = req.body.email
      user.name = req.body.name
      user.setPassword(req.body.password)

      user.save((err, user) => {
        if (err) {
          if (err.code === 11000) {
            res.status(401).send({ email: 'Пользователь с таким email уже существует' })
          } else {
            console.log(err)
            res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
          }
        }
        const token = user.generateJwt()
        res.status(200).send({ token })
      })
    }
  })
}

const login = (req, res) => {
  passport.authenticate('local', (err, user, info) => {
    let token
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else if (!user) {
      res.status(401).send(info)
    } else if (user) {
      token = user.generateJwt()
      res.status(200).send({ token })
    }
  })(req, res)
}

const forget = (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (err) {
      console.log(err)
      res.status(401).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else if (user) {
      const link = `https://${Configs.serverData.addressProd}:${Configs.serverData.port}/#/newpass?user=${user.email}}`
      const emailText = `Здравствуйте, ${user.name} <br><br> В системе NextStep (nextstep.ml) была запрошена информация для смены вашего пароля. <br><br> Для изменения пароля, пожалуйста, перейдите по ссылке:
        ${link} <br><br> С уважением, команда проекта NextStep`
      mailsender.sendMail(user.email, 'Смена пароля NextStep', emailText, emailText, (err, message) => {
        if (err) res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
        else res.status(200).end()
      })
    } else res.status(401).send({ email: 'Пользователь с таким email не найден' })
  })
}

const check = (req, res) => {
  User.findOne({ email: req.params.email }, (err, user) => {
    if (err) {
      console.log(err)
      res.status(401).end()
    } else if (user) res.status(200).end()
    else res.status(401).end()
  })
}

const change = (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else {
      user.setPassword(req.body.pass)
      user.save(err => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
        } else res.status(200).end()
      })
    }
  })
}

const createAdmin = () => {
  User.findOne({ role: 'admin' }, (err, u) => {
    if (err) {
      console.log(err)
    } else if (!u) {
      const user = new User()
      user.username = 'admin'
      user.email = 'ok@automato.me'
      user.name = 'Admin'
      user.role = 'admin'
      user.setPassword('12345678')

      user.save(err => {
        if (err) console.log(err)
      })
    }
  })
}

module.exports = {
  signup,
  login,
  forget,
  change,
  check,
  createAdmin
}
