import Goal from '../models/Goal'

const get = (req, res) => {
  Goal.find({ book: req.params.id, user: req.payload._id }, (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(list)
  })
}

const add = (req, res) => {
  const goal = new Goal()
  goal.text = req.body.text
  goal.user = req.payload._id
  goal.book = req.params.id
  goal.save((err, goal) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(goal)
  })
}

const edit = (req, res) => {
  Goal.findOneAndUpdate({ _id: req.body._id }, { $set: { text: req.body.text }}, (err, goal) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(goal)
  })
}

const remove = (req, res) => {
  Goal.findOneAndRemove({ _id: req.body.goal }, (err) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).end()
  })
}

const getAll = (req, res) => {
  Goal.find({ user: req.payload._id }).sort('-createdAt').exec((err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(list)
  })
}

module.exports = {
  get,
  add,
  edit,
  remove,
  getAll
}
