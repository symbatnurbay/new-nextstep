import Course from '../models/Course'
import Book from '../models/Book'
import Video from '../models/Video'
import UserCourse from '../models/UserCourse'
import fs from 'fs'
import path from 'path'

// Добавление нового курса
const add = (req, res) => {
  const course = new Course()
  course.name = req.body.name
  course.category = req.body.category
  course.currency = JSON.parse(req.body.currency)
  course.price = req.body.price
  course.description = req.body.description
  course.logo = `images/courses/${req.file.filename}`
  course.save((err) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send()
  })
}

// Вывод списка курсов
const get = (req, res) => {
  Course.find({ category: req.params.id }, (err, courses) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Book.aggregate([{ $group: { _id: '$course', count: { $sum: 1 }}}], (err, books) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          Video.aggregate([{ $group: { _id: '$course', count: { $sum: 1 }}}], (err, videos) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else {
              var temp = -1
              var temp2 = -1
              var list = []
              courses.forEach((item, index) => {
                list.push(item.toObject())
                temp = books.findIndex(x => x._id.toString() === item._id.toString())
                temp2 = videos.findIndex(x => x._id.toString() === item._id.toString())
                if (temp > -1) list[index].book = books[temp].count
                if (temp2 > -1) list[index].video = videos[temp2].count
              })
              if (req.payload.role === 'admin') {
                res.status(200).send({ list })
              } else if (req.payload.role === 'user') {
                UserCourse.find({ user: req.payload._id, active: true }, (err, access) => {
                  if (err) {
                    console.log(err)
                    res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                  } else res.status(200).send({ list, access })
                })
              }
            }
          })
        }
      })
    }
  })
}

// Редактирование курса
const edit = (req, res) => {
  Course.findById(req.params.id, (err, course) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      course.category = course.category !== req.body.category ? req.body.category : course.category
      course.currency = course.currency.name !== JSON.parse(req.body.currency).name ? JSON.parse(req.body.currency) : course.currency
      course.name = course.name !== req.body.name ? req.body.name : course.name
      course.price = course.price !== req.body.price ? req.body.price : course.price
      course.description = course.description !== req.body.description ? req.body.description : course.description
      if (req.file) course.logo = `images/courses/${req.file.filename}`
      course.save((err, course) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          course.populate('category', (err) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else res.status(200).send(course)
          })
        }
      })
    }
  })
}

// Получение информациии одного курса по id
const check = (req, res) => {
  Course.findById(req.params.id).populate('category').exec((err, course) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(course)
  })
}

const remove = (req, res) => {
  UserCourse.update({ course: req.params.id }, { $set: { active: false }}, (err) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      require('./book').removeMany(req.params.id, err => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          require('./video').removeMany(req.params.id, err => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else {
              Course.findByIdAndRemove(req.params.id, (err, course) => {
                if (err) {
                  console.log(err)
                  res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                } else {
                  fs.unlink(path.join('./static', course.logo), err => {
                    if (err) {
                      console.log(err)
                      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                    } else res.status(200).end()
                  })
                }
              })
            }
          })
        }
      })
    }
  })
}

const getMy = (req, res) => {
  UserCourse.find({ user: req.payload._id, active: true }, (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      var tempArr = []
      result.forEach(item => {
        tempArr.push(item.course)
      })
      Course.find({ _id: { $in: tempArr }}, (err, courses) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          Book.aggregate([{ $group: { _id: '$course', count: { $sum: 1 }}}], (err, books) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else {
              Video.aggregate([{ $group: { _id: '$course', count: { $sum: 1 }}}], (err, videos) => {
                if (err) {
                  console.log(err)
                  res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                } else {
                  var temp = -1
                  var temp2 = -1
                  var list = []
                  courses.forEach((item, index) => {
                    list.push(item.toObject())
                    temp = books.findIndex(x => x._id.toString() === item._id.toString())
                    temp2 = videos.findIndex(x => x._id.toString() === item._id.toString())
                    if (temp > -1) list[index].book = books[temp].count
                    if (temp2 > -1) list[index].video = videos[temp2].count
                  })
                  res.status(200).send({ list, result })
                }
              })
            }
          })
        }
      })
    }
  })
}

module.exports = {
  get,
  add,
  edit,
  check,
  remove,
  getMy
}
