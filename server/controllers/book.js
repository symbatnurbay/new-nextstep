import Book from '../models/Book'
import Goal from '../models/Goal'
import Category from '../models/Category'
import fs from 'fs'
import path from 'path'

const add = (req, res) => {
  Book.count({ course: req.body.course }, (err, number) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      const book = new Book()
      book.name = req.body.name
      book.author = req.body.author
      book.pages = req.body.pages
      book.description = req.body.description
      book.number = number + 1
      book.course = req.body.course
      book.link = req.body.link
      book.year = req.body.year
      book.photo = `/images/books/${req.file.filename}`
      book.save((err) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send()
      })
    }
  })
}

const edit = (req, res) => {
  Book.findById(req.params.id, (err, book) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      book.name = book.name !== req.body.name ? req.body.name : book.name
      book.author = book.author !== req.body.author ? req.body.author : book.author
      book.pages = book.pages !== req.body.pages ? req.body.pages : book.pages
      book.description = book.description !== req.body.description ? req.body.description : book.description
      book.course = book.course !== req.body.course ? req.body.course : book.course
      book.link = book.link !== req.body.link ? req.body.link : book.link
      book.year = book.year !== req.body.year ? req.body.year : book.year
      console.log(req.file)
      if (req.file) book.photo = `/images/books/${req.file.filename}`
      book.save((err, book) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send()
      })
    }
  })
}

// Получение информации об одном видео
const check = (req, res) => {
  Book.findById(req.params.id).populate('course', '_id name category logo').exec((err, book) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Category.findById(book.course.category, { name: 1 }, (err, category) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          var temp = book.toObject()
          temp.category = category
          res.status(200).send(temp)
        }
      })
    }
  })
}

const list = (req, res) => {
  Book.find({ course: req.params.id }, {}, { sort: { number: 1 }}, (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(list)
  })
}

const next = (req, res) => {
  Book.findById(req.params.id, (err, video) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Book.findOne({ course: video.course, number: video.number + 1 }, (err, result) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(result)
      })
    }
  })
}

const prev = (req, res) => {
  Book.findById(req.params.id, (err, video) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Book.findOne({ course: video.course, number: video.number - 1 }, (err, result) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(result)
      })
    }
  })
}

const remove = (req, res) => {
  Goal.remove({ book: req.params.id }, (err) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Book.findByIdAndRemove(req.params.id, (err, book) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          fs.unlink(path.join('./static', book.photo), err => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else res.status(200).end()
          })
        }
      })
    }
  })
}

const removeMany = (course, clback) => {
  Book.find({ course }, { _id: 1, photo: 1 }, (err, list) => {
    if (err) clback(err)
    else {
      Goal.remove({ book: { $in: list }}, (err) => {
        if (err) clback(err)
        else {
          list.forEach(item => {
            fs.unlink(path.join('./static', item.photo), err => {
              if (err) clback(err)
            })
          })
          Book.remove({ course }, (err) => {
            if (err) clback(err)
            else clback(null)
          })
        }
      })
    }
  })
}

module.exports = {
  check,
  add,
  edit,
  list,
  next,
  prev,
  remove,
  removeMany
}
