import Category from '../models/Category'
import Course from '../models/Course'
import UserCourse from '../models/UserCourse'
import async from 'async'
import fs from 'fs'
import path from 'path'

const add = (req, res) => {
  const category = new Category()
  category.name = req.body.name
  category.photo = `images/categories/${req.file.filename}`
  category.save((err, newCategory) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(newCategory)
  })
}

const get = (req, res) => {
  Category.find((err, categories) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Course.aggregate([{ $group: { _id: '$category', count: { $sum: 1 }}}], (err, result) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          var temp = -1
          var list = []
          categories.forEach((item, index) => {
            list.push(item.toObject())
            temp = result.findIndex(x => x._id.toString() === item._id.toString())
            if (temp > -1) list[index].course = result[temp].count
          })
          res.status(200).send(list)
        }
      })
    }
  })
}

// Изменение  категории
const edit = (req, res) => {
  // Вывбор нужной категории из базы
  Category.findById(req.params.id, (err, category) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте поже' }).end()
    } else {
      // Проверка на отсутствие фотографии
      if (req.file) category.photo = `images/categories/${req.file.filename}`
      category.name = category.name !== req.body.name ? req.body.name : category.name
      category.save((err, category) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(category)
      })
    }
  })
}

const remove = (req, res) => {
  Course.find({ category: req.params.id }, (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте поже' }).end()
    } else {
      async.forEachOf(list, (value, key, callback) => {
        require('./book').removeMany(value._id, err => {
          if (err) callback(err)
          else {
            require('./video').removeMany(value._id, err => {
              if (err) callback(err)
              else {
                UserCourse.update({ course: value._id }, { $set: { active: false }}, (err) => {
                  if (err) callback(err)
                  else {
                    Course.findByIdAndRemove(value._id, (err, course) => {
                      if (err) callback(err)
                      else {
                        fs.unlink(path.join('./static', course.logo), err => {
                          if (err) callback(err)
                          else callback(null)
                        })
                      }
                    })
                  }
                })
              }
            })
          }
        })
      }, err => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте поже' }).end()
        } else {
          Category.findByIdAndRemove(req.params.id, (err, category) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте поже' }).end()
            } else {
              fs.unlink(path.join('./static', category.photo), err => {
                if (err) {
                  console.log(err)
                  res.status(400).send({ message: 'Неизвестная ошибка, попробуйте поже' }).end()
                } else res.status(200).end()
              })
            }
          })
        }
      })
    }
  })
}

const one = (req, res) => {
  Category.findById(req.params.id, (err, category) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте поже' }).end()
    } else res.status(200).send(category)
  })
}

module.exports = {
  add,
  get,
  edit,
  remove,
  one
}
