import User from '../models/user'
import Course from '../models/Course'
import Category from '../models/Category'
import fs from 'fs'
import path from 'path'

const current = (req, res) => {
  User.findById(req.payload._id, (err, user) => {
    if (err) {
      console.log(err)
      res.status(400).end()
    } else res.status(200).send(user).end()
  })
}

const edit = (req, res) => {
  User.findById(req.payload._id, (err, user) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось обновить данные, попробуйте позже' }).end()
    } else {
      user.name = req.body.name || user.name
      user.birthdate = req.body.birthdate || user.birthdate
      user.city = req.body.city || user.city
      user.phone = req.body.phone || user.phone
      if (req.body.password) user.setPassword(req.body.password)
      user.save((err, user) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Не удалось обновить данные, попробуйте позже' }).end()
        } else res.status(200).send(user)
      })
    }
  })
}

const getAll = (req, res) => {
  User.aggregate([
    { $match: { status: 'active', role: 'user' }},
    {
      $lookup: {
        from: 'usercourses',
        as: 'result',
        localField: '_id',
        foreignField: 'user'
      }
    },
    {
      $project: {
        _id: 1,
        name: 1,
        email: 1,
        birthdate: 1,
        phone: 1,
        avatar: 1,
        city: 1,
        courses: {
          $map: {
            input: {
              $filter: {
                input: '$result',
                as: 'item',
                cond: { $eq: ['$$item.active', true] }
              }
            },
            as: 'm',
            in: '$$m.course'
          }
        }
      }
    }
  ], (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Course.populate(result, { path: 'courses', select: '_id logo name category' }, (err, list) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          Category.populate(list, { path: 'courses.category', select: '_id name' }, (err, users) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else res.status(200).send(users)
          })
        }
      })
    }
  })
}

const changeAvatar = (req, res) => {
  const avatar = `/images/avatars/${req.file.filename}`
  User.findById(req.params.id, (err, data) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось изменить фото, попробуйте позже' }).end()
    } else {
      if (data.avatar !== '/images/photo-placeholder.png') {
        fs.unlink(path.join('./static', data.avatar), err => {
          if (err) console.log(err)
        })
      }
      data.avatar = avatar
      data.save((err, user) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(user)
      })
    }
  })
}

const deleteImage = (req, res) => {
  User.findById(req.params.id, (err, data) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось изменить фото, попробуйте позже' }).end()
    } else {
      fs.unlink(path.join('./static', data.avatar), err => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Не удалось изменить фото, попробуйте позже' }).end()
        } else {
          data.avatar = '/images/photo-placeholder.png'
          data.save((err, user) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Не удалось изменить фото, попробуйте позже' }).end()
            } else res.status(200).send(user)
          })
        }
      })
    }
  })
}

module.exports = {
  current,
  edit,
  getAll,
  changeAvatar,
  deleteImage
}
