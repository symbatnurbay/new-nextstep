import request from 'request'
import btoa from 'btoa'
import Configs from '../config/config'
import UserCourse from '../models/UserCourse'
const querystring = require('querystring')

const pay = (req, res) => {
  request.post({
    url: 'https://api.cloudpayments.ru/payments/cards/charge',
    headers: {
      'Authorization': 'Basic ' + btoa(`${Configs.cloudPayment.id}:${Configs.cloudPayment.password}`)
    },
    body: {
      'Amount': req.body.course.price,
      'Currency': req.body.course.currency.code,
      'InvoiceId': '1234567',
      'Description': `Подписка на курс ${req.body.course.name}`,
      'AccountId': 'user_x',
      'Name': req.body.card.name,
      'Email': req.payload.email,
      'CardCryptogramPacket': req.body.cryptogram
    },
    json: true
  }, (err, response, body) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: body.Model.CardHolderMessage }).end()
    } else {
      const user = new UserCourse()
      user.user = req.payload._id
      user.course = req.body.course._id
      user.transactionId = body.Model.TransactionId
      user.active = body.Success
      user.cardNumber = req.body.card.number
      user.cardHolder = req.body.card.name
      user.save((err, user) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Данные введены неправильно' }).end()
        } else {
          if (body.Success) res.status(200).send({ done: true, user })
          else res.status(200).send(body.Model)
        }
      })
    }
  })
}

const finish = (req, res) => {
  request.post({
    url: 'https://api.cloudpayments.ru/payments/cards/post3ds',
    headers: {
      'Authorization': 'Basic ' + btoa(`${Configs.cloudPayment.id}:${Configs.cloudPayment.password}`)
    },
    body: {
      'TransactionId': req.body.MD,
      'PaRes': req.body.PaRes
    },
    json: true
  }, (err, response, body) => {
    if (err) {
      console.log(err)
      const query = querystring.stringify({
        'message': body.Model.CardHolderMessage
      })
      res.redirect(`/#/category/${req.params.id}/error?${query}`)
    } else {
      if (body.Success) {
        UserCourse.findOneAndUpdate({ transactionId: req.body.MD }, { active: true }, (err, result) => {
          if (err) {
            console.log(err)
            const query = querystring.stringify({
              'message': 'Неизвестная ошибка, попробуйте позже'
            })
            res.redirect(`/#/category/${req.params.id}/error?${query}`)
          } else res.redirect(`/#/category/${req.params.id}/success`)
        })
      } else {
        const query = querystring.stringify({
          'message': body.Model.CardHolderMessage
        })
        res.redirect(`/#/category/${req.params.id}/error?${query}`)
      }
    }
  })
}

module.exports = {
  pay,
  finish
}
