import Video from '../models/Video'
import Category from '../models/Category'
import fs from 'fs'
import path from 'path'

const add = (req, res) => {
  console.log(req.files)
  Video.count({ course: req.body.course }, (err, number) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      const video = new Video()
      video.name = req.body.name
      video.duration = req.body.duration
      video.author = req.body.author
      video.description = req.body.description
      video.number = number + 1
      video.course = req.body.course
      video.tasks = req.body.tasks
      video.link = `/video/${req.files.video[0].filename}`
      video.code = `/files/${req.files.files[0].filename}`
      // video.link = req.body.link
      video.save((err) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send()
      })
    }
  })
}

const edit = (req, res) => {
  Video.findById(req.params.id, (err, video) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      video.name = video.name !== req.body.name ? req.body.name : video.name
      video.duration = video.duration !== req.body.duration ? req.body.duration : video.duration
      video.author = video.author !== req.body.author ? req.body.author : video.author
      video.description = video.description !== req.body.description ? req.body.description : video.description
      video.course = video.course !== req.body.course ? req.body.course : video.course
      video.tasks = video.tasks !== req.body.tasks ? req.body.tasks : video.tasks
      if (req.files.video) {
        fs.unlink(path.join('./static', video.link), err => {
          if (err) {
            console.log(err)
            // res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
          }
        })
      }
      if (req.files.files) {
        fs.unlink(path.join('./static', video.code), err => {
          if (err) {
            console.log(err)
            // res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
          }
        })
      }
      video.link = req.files.video ? `/video/${req.files.video[0].filename}` : video.link
      video.code = req.files.files ? `/files/${req.files.files[0].filename}` : video.code

      video.save((err, video) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send()
      })
    }
  })
}

// Получение информации об одном видео
const check = (req, res) => {
  Video.findById(req.params.id).populate('course', '_id name category logo').exec((err, video) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Category.findById(video.course.category, { name: 1 }, (err, category) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          var temp = video.toObject()
          temp.category = category
          res.status(200).send(temp)
        }
      })
    }
  })
}

const list = (req, res) => {
  Video.find({ course: req.params.id }, {}, { sort: { number: 1 }}, (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(list)
  })
}

const next = (req, res) => {
  Video.findById(req.params.id, (err, video) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Video.findOne({ course: video.course, number: video.number + 1 }, (err, result) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(result)
      })
    }
  })
}

const prev = (req, res) => {
  Video.findById(req.params.id, (err, video) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      Video.findOne({ course: video.course, number: video.number - 1 }, (err, result) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(result)
      })
    }
  })
}

const remove = (req, res) => {
  Video.findByIdAndRemove(req.params.id, (err, video) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      fs.unlink(path.join('./static', video.link), err => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).end()
      })
    }
  })
}

const removeMany = (course, clback) => {
  Video.find({ course }, (err, list) => {
    if (err) clback(err)
    else {
      list.forEach(item => {
        fs.unlink(path.join('./static', item.link), err => {
          if (err) clback(err)
        })
      })
      Video.remove({ course }, (err) => {
        if (err) clback(err)
        else clback(null)
      })
    }
  })
}

module.exports = {
  check,
  add,
  edit,
  list,
  next,
  prev,
  remove,
  removeMany
}
