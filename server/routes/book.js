import express from 'express'
const router = new express.Router()

import multer from 'multer'
import path from 'path'
import { admin, book, course } from '../middlewares/index'

const storage = multer.diskStorage({
  destination: 'static/images/books',
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage })
import ctrlBook from '../controllers/book'

// router.get('/get/:id', ctrlCourses.get)
router.post('/add', admin, upload.single('photo'), ctrlBook.add)
router.post('/edit/:id', admin, upload.single('photo'), ctrlBook.edit)
router.get('/check/:id', book, ctrlBook.check)
router.get('/list/:id', course, ctrlBook.list)
router.get('/next/:id', book, ctrlBook.next)
router.get('/prev/:id', book, ctrlBook.prev)
router.delete('/:id', admin, ctrlBook.remove)

module.exports = router
