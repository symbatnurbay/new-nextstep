import express from 'express'
const router = new express.Router()

import ctrlAuth from '../controllers/authentication'

router.post('/signup', ctrlAuth.signup)
router.post('/login', ctrlAuth.login)
router.post('/forget', ctrlAuth.forget)
router.get('/check/:email', ctrlAuth.check)
router.post('/change', ctrlAuth.change)

module.exports = router
