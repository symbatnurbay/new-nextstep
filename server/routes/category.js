import express from 'express'
const router = new express.Router()

import multer from 'multer'
import path from 'path'
import { admin } from '../middlewares/index'

const storage = multer.diskStorage({
  destination: 'static/images/categories',
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage })

import ctrlCategory from '../controllers/category'

router.post('/add', admin, upload.single('photo'), ctrlCategory.add)
router.get('/get', ctrlCategory.get)
router.post('/edit/:id', admin, upload.single('photo'), ctrlCategory.edit)
router.delete('/:id', admin, ctrlCategory.remove)
router.get('/one/:id', ctrlCategory.one)

module.exports = router
