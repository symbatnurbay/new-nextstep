import express from 'express'
const router = new express.Router()

import multer from 'multer'
import path from 'path'
import { admin, course } from '../middlewares/index'

const storage = multer.diskStorage({
  destination: 'static/images/courses',
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage })
import ctrlCourses from '../controllers/course'
import ctrlPayment from '../controllers/payment'

router.get('/get/:id', ctrlCourses.get)
router.get('/my', ctrlCourses.getMy)
router.post('/add', admin, upload.single('logo'), ctrlCourses.add)
router.post('/edit/:id', admin, upload.single('logo'), ctrlCourses.edit)
router.get('/check/:id', course, ctrlCourses.check)
router.delete('/:id', admin, ctrlCourses.remove)
router.post('/pay', ctrlPayment.pay)

module.exports = router
