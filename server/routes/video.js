import express from 'express'
const router = new express.Router()

import multer from 'multer'
import path from 'path'
import { admin, video, course } from '../middlewares/index'

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    const link = `./static/${file.fieldname}`
    callback(null, link)
  },
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage }).fields([{ name: 'video', maxCount: 1 }, { name: 'files', maxCount: 1 }])
import ctrlVideo from '../controllers/video'

// router.get('/get/:id', ctrlCourses.get)
router.post('/add', admin, upload, ctrlVideo.add)
// router.post('/add', admin, ctrlVideo.add)
router.post('/edit/:id', admin, upload, ctrlVideo.edit)
router.get('/check/:id', video, ctrlVideo.check)
router.get('/list/:id', course, ctrlVideo.list)
router.get('/next/:id', video, ctrlVideo.next)
router.get('/prev/:id', video, ctrlVideo.prev)
router.delete('/:id', admin, ctrlVideo.remove)

module.exports = router
