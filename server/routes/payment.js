import express from 'express'
const router = new express.Router()

import ctrlPayment from '../controllers/payment'

router.post('/finish/:id', ctrlPayment.finish)

module.exports = router
