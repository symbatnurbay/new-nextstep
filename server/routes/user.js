import express from 'express'
const router = new express.Router()

import multer from 'multer'
import path from 'path'

const storage = multer.diskStorage({
  destination: 'static/images/avatars',
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage }).single('avatar')

import ctrlUser from '../controllers/user'
import { admin } from '../middlewares/index'

router.get('/current', ctrlUser.current)
router.get('/all', admin, ctrlUser.getAll)
router.post('/edit', ctrlUser.edit)
router.post('/avatar/:id', upload, ctrlUser.changeAvatar)
router.delete('/avatar/:id', ctrlUser.deleteImage)

module.exports = router
