import express from 'express'
const router = express.Router()
import { auth } from '../middlewares/index'

router.use('/auth', require('./authentication'))
router.use('/category', auth, require('./category'))
router.use('/user', auth, require('./user'))
router.use('/course', auth, require('./course'))
router.use('/video', auth, require('./video'))
router.use('/book', auth, require('./book'))
router.use('/goals', auth, require('./goals'))
router.use('/payment', require('./payment'))
// router.use('/tickets', auth, require('./tickets'))
// router.use('/history', auth, require('./history'))
// router.use('/notification', auth, require('./notification'))

// если запрос под никакой апи не подходит, то оставим это дело ангуляр
router.get('*', (req, res) => {
  res.redirect('/#' + req.originalUrl)
})

module.exports = router

// 400 - unknown error
// 401 - unauthorized / invalid token
// 403 - forbidden
