import express from 'express'
const router = new express.Router()

import { book } from '../middlewares/index'

import ctrlGoals from '../controllers/goals'

router.get('/get/:id', book, ctrlGoals.get)
router.get('/all', ctrlGoals.getAll)
router.post('/add/:id', book, ctrlGoals.add)
router.post('/edit/:id', book, ctrlGoals.edit)
router.post('/remove/:id', book, ctrlGoals.remove)

module.exports = router
