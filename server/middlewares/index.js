import User from '../models/user'
import Video from '../models/Video'
import Book from '../models/Book'
import UserCourse from '../models/UserCourse'
import Configs from '../config/config'
import jwt from 'jsonwebtoken'

const auth = (req, res, next) => {
  const token = req.headers['authorization']
  if (!token) res.status(401).send({ message: 'Invalid token' }).end()
  else {
    const q = token.split(' ')[1]
    jwt.verify(q, Configs.auth.JWT_SECRET, (err, result) => {
      if (err) {
        console.log(err)
        res.status(401).send({ message: 'Invalid token' }).end()
      } else {
        User.findOne({ _id: result._id, email: result.email, username: result.username, role: result.role }, (err, user) => {
          if (err) {
            console.log(err)
            res.status(401).send({ message: 'Invalid token' }).end()
          } else if (!user) res.status(401).send({ message: 'Invalid token' }).end()
          else {
            req.payload = result
            next()
          }
        })
      }
    })
  }
}

const admin = (req, res, next) => {
  if (req.payload && req.payload.role === 'admin') next()
  else res.status(403).send({ message: 'Отказано в доступе' }).end()
}

const course = (req, res, next) => {
  if (req.payload) {
    if (req.payload.role === 'admin') next()
    else {
      UserCourse.findOne({ user: req.payload._id, course: req.params.id }, (err, result) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else if (result) next()
        else res.status(403).send({ message: 'Отказано в доступе' }).end()
      })
    }
  } else res.status(403).send({ message: 'Отказано в доступе' }).end()
}

const video = (req, res, next) => {
  if (req.payload) {
    if (req.payload.role === 'admin') next()
    else {
      Video.findById(req.params.id, (err, video) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          UserCourse.findOne({ user: req.payload._id, course: video.course }, (err, result) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else if (result) next()
            else res.status(403).send({ message: 'Отказано в доступе' }).end()
          })
        }
      })
    }
  } else res.status(403).send({ message: 'Отказано в доступе' }).end()
}

const book = (req, res, next) => {
  if (req.payload) {
    if (req.payload.role === 'admin') next()
    else {
      Book.findById(req.params.id, (err, book) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          UserCourse.findOne({ user: req.payload._id, course: book.course }, (err, result) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else if (result) next()
            else res.status(403).send({ message: 'Отказано в доступе' }).end()
          })
        }
      })
    }
  } else res.status(403).send({ message: 'Отказано в доступе' }).end()
}

module.exports = {
  auth,
  admin,
  course,
  video,
  book
}
