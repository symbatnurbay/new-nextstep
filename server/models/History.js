import mongoose from 'mongoose'

const historySchema = mongoose.Schema({
  text: String,
  type: String,
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  }
},
{
  timestamps: true
})

module.exports = mongoose.model('History', historySchema)
