import mongoose from 'mongoose'

const bookSchema = mongoose.Schema({
  name: String,
  pages: Number,
  author: String,
  description: String,
  number: Number,
  course: {
    type: mongoose.Schema.ObjectId,
    ref: 'Course'
  },
  photo: String,
  link: String,
  year: String
},
{
  timestamps: true
})

module.exports = mongoose.model('Book', bookSchema)
