import mongoose from 'mongoose'
import crypto from 'crypto'
import jwt from 'jsonwebtoken'
import Configs from '../config/config'

const userSchema = mongoose.Schema({
  name: String,
  email: {
    type: String,
    unique: true
  },
  username: {
    type: String,
    unique: true
  },
  hash: String,
  salt: String,
  role: {
    type: String,
    default: 'user'
  },
  birthdate: Date,
  phone: String,
  avatar: {
    type: String,
    default: '/images/photo-placeholder.png'
  },
  city: String,
  status: {
    type: String,
    default: 'active'
  }
},
{
  timestamps: true
})

// Функция для создания пароля
userSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex')
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex')
}

// Функция для проверки валидности пароля
userSchema.methods.validPassword = function (password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex')
  return this.hash === hash
}

// генерирует JsonWebToken, ставит срок истечения на 7 дней
userSchema.methods.generateJwt = function () {
  const payload = {
    _id: this._id,
    email: this.email,
    username: this.username,
    role: this.role
  }
  return jwt.sign(payload, Configs.auth.JWT_SECRET, { expiresIn: Configs.auth.tokenExpiry })
}

userSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    var retJson = {
      _id: ret._id,
      email: ret.email,
      username: ret.username,
      status: ret.status,
      name: ret.name,
      city: ret.city,
      avatar: ret.avatar,
      role: ret.role,
      birthdate: ret.birthdate,
      phone: ret.phone
    }
    return retJson
  }
})

module.exports = mongoose.model('User', userSchema)
