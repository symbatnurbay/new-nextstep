import mongoose from 'mongoose'

const courseSchema = mongoose.Schema({
  name: String,
  price: Number,
  currency: Object,
  category: {
    type: mongoose.Schema.ObjectId,
    ref: 'Category'
  },
  description: String,
  logo: String
},
{
  timestamps: true
})

module.exports = mongoose.model('Course', courseSchema)
