import mongoose from 'mongoose'

const categorySchema = mongoose.Schema({
  name: String,
  photo: String
},
{
  timestamps: true
})

module.exports = mongoose.model('Category', categorySchema)
