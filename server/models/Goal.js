import mongoose from 'mongoose'

const goalSchema = mongoose.Schema({
  text: String,
  book: {
    type: mongoose.Schema.ObjectId,
    ref: 'Book'
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  }
},
{
  timestamps: true
})

module.exports = mongoose.model('Goal', goalSchema)
