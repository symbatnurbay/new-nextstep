import mongoose from 'mongoose'

const videoSchema = mongoose.Schema({
  name: String,
  duration: String,
  author: String,
  description: String,
  number: Number,
  course: {
    type: mongoose.Schema.ObjectId,
    ref: 'Course'
  },
  tasks: Array,
  link: String,
  code: String
},
{
  timestamps: true
})

module.exports = mongoose.model('Video', videoSchema)
