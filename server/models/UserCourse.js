import mongoose from 'mongoose'

const userCourseSchema = mongoose.Schema({
  course: {
    type: mongoose.Schema.ObjectId,
    ref: 'Course'
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  transactionId: String,
  cardHolder: String,
  cardNumber: String,
  active: Boolean
},
{
  timestamps: true
})

module.exports = mongoose.model('UserCourse', userCourseSchema)
